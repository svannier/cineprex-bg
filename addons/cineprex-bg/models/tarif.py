from odoo import models, fields, api


class Tarif(models.Model):
    _name = "cineprex.tarif"
    _rec_name = 'nom'
    _description = "Réduction applicable en %."
    currency_id = fields.Many2one('res.currency', string='Currency')

    prix = fields.Monetary('Prix')
    
    nom = fields.Char("Nom")