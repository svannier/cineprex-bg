from odoo import models, fields, api


class Salle(models.Model):
    _name = "cineprex.salle"
    _rec_name = 'information'
    _description = "Une salle noire"

    salle_id = fields.Integer("ID")
    nb_de_place = fields.Integer('Places Disponibles')
    type_salle = fields.Selection(
        [('GRANDE_SALLE', 'Grande Salle'), ('PETITE_SALLE', 'Petite salle'), ('DOLBY_ATMOS', 'Dolby Atmos'), ('4K', '4K'), ('ICE', 'ICE')])

    id_site = fields.Many2one(comodel_name="cineprex.site")
    id_seance = fields.One2many(
        comodel_name="cineprex.seance", inverse_name="id_salle")

    information = fields.Char(string="Information", compute="_getname")

    @api.depends('id_site', 'salle_id')
    def _getname(self):
        for salle in self:
            salle.information = f"[ {salle.id_site.nom} ] Salle n°{salle.id}"
