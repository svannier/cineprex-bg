from odoo import models, fields


class Site(models.Model):
    _name = 'cineprex.site'
    _rec_name = 'nom'
    _description = "Site de cinéma"

    site_id = fields.Integer('ID')
    nom = fields.Char("Nom")
    localisation = fields.Text("Localisation")
    photo = fields.Image("Photo")

    id_salle = fields.One2many(comodel_name="cineprex.salle", inverse_name="id_site")

    films = fields.Many2many(comodel_name="cineprex.film")
