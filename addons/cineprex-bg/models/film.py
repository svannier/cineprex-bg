from odoo import models, fields

class Film(models.Model):
    _name = 'cineprex.film'
    _rec_name = "nom"
    _description = "Un film diffuser ou diffusable"

    film_id = fields.Integer("ID")

    nom = fields.Char("Nom")
    realisateur = fields.Char("Realisateur")
    annee = fields.Date("Date de sortie")
    duree = fields.Float('Durée')
    affiche = fields.Image('Affiche')

    tout_public = fields.Boolean("Film tout public")

    id_seance = fields.One2many(comodel_name="cineprex.seance", inverse_name="id_film")

    cinemas = fields.Many2many(comodel_name="cineprex.site")
