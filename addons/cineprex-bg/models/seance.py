from odoo import models, fields, api

class Seance(models.Model):
    _name = 'cineprex.seance'
    _rec_name = 'information'
    _description = "Seance de cinema un film + Salle + horaire"

    seance_id = fields.Integer("ID")
    date_start = fields.Datetime("Début Seance")
    date_end = fields.Datetime("Fin Seance")

    id_salle = fields.Many2one(comodel_name='cineprex.salle')
    id_film = fields.Many2one(comodel_name='cineprex.film')

    information = fields.Char(string="Information", compute="_getname")

    place_vendue = fields.One2many(comodel_name="cineprex.billet", inverse_name="seance")

    @api.depends('id_film', 'id_salle', 'seance_id')
    def _getname(self):
        for seance in self:
            seance.information = f"[ {seance.id_film.nom} ] Salle n°{seance.id_salle.salle_id} le {seance.date_start}"
