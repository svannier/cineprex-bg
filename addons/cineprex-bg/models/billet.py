from odoo import models, fields, api


class Billet(models.Model):
    _name = "cineprex.billet"
    _description = "Billet pour une séance"
    
    seance = fields.Many2one(comodel_name='cineprex.seance')
    tarif = fields.Many2one(comodel_name='cineprex.tarif')