import logging

from odoo import http
from odoo.http import request
import logging

_logger = logging.getLogger(__name__)

class Cineprex(http.Controller):

    @http.route(['''/cinemas'''], type='http', auth="public", website=True, sitemap=True)
    def cineprex_cinemas(self, **kw):
        sites = http.request.env['cineprex.site'].search([])
        return request.render("cineprex-bg.cineprex-cinemas", {"sites": sites})

    @http.route(['''/cinemas/<int:cinema_id>'''], type='http', auth="public", website=True, sitemap=True)
    def cineprex_cinema(self, cinema_id, **kw):
        cinema = http.request.env['cineprex.site'].browse(cinema_id)
        # get salle
        salle_id = cinema.read(["id_salle"])[0]["id_salle"]
        salles = http.request.env['cineprex.salle'].browse(salle_id)
        return request.render("cineprex-bg.cineprex-cinema", {"cinema": cinema, "salles": salles})


    @http.route(['''/films'''], type='http', auth="public", website=True, sitemap=True)
    def cineprex_films(self, **kw):
        films = http.request.env['cineprex.film'].search([])
        return request.render("cineprex-bg.cineprex-films", {"films": films})


    @http.route(['''/salles/<int:cinema_id>'''], type='http', auth="public", website=True, sitemap=True)
    def cineprex_salles_for_cinema(self, cinema_id, **kw):
        cinema = http.request.env['cineprex.site'].browse(cinema_id)
        # get salle
        salle_id = cinema.read(["id_salle"])[0]["id_salle"]
        salles = http.request.env['cineprex.salle'].browse(salle_id)

        return request.render("cineprex-bg.cineprex-salles", {"salles": salles})