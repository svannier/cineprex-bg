# -*- coding: utf-8 -*-
{
    'name': "Cineprex BG",
    'version': '0.1',
    'depends': ['base'],
    'author': ["Clément Duez, Hugo Porchet-Belmonte, Samuel Vannier"],
    'website': "https://www.univ-larochelle.fr/",
    'category': 'Project Management',
    'description': """
    Cineprex BG le module pour les BG cinemas.
    """,
    'depends': [
        'base',
        'website_sale',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/film_view.xml',
        'views/salle_view.xml',
        'views/seance_view.xml',
        'views/site_view.xml',
        'views/tarif_view.xml',
        'views/billet_view.xml',
        'templates/cinemas_template.xml',
        'templates/cinema_template.xml',
        'templates/films_template.xml',
        'templates/salles_template.xml',
    ],
    'application': True,
    'installable': True
}