# CinéPrex - Projet

## Membres

 * Clément Duez
 * Samuel Vannier
 * Hugo Porchet-Belmonte

## Build application

````shell
docker-compose build
````
add ``,/mnt/extra-addons`` in the addons_path of ``conf/odoo.conf`` 

## Start application

````shell
docker-compose up
````
Go to [localhost](http://localhost:8069)